/**
 * 
 */

package edu.towson.cis.cosc442.project4.coffeemaker;

import junit.framework.TestCase;

public class RecipeTest extends TestCase{
	Recipe r1 = new Recipe();
	Recipe r2 = new Recipe();
	
	
	public void testEquals(){
		r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(30);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(5);
		r1.setAmtChocolate(0);
		
		r2 = new Recipe();
		r2.setName("Coffee2");
		r2.setPrice(30);
		r2.setAmtCoffee(4);
		r2.setAmtMilk(1);
		r2.setAmtSugar(8);
		r2.setAmtChocolate(4);
		
		//r1.equals(r2);
		assertFalse(r1.equals(r2));
	}
	
	
	public void testSetPrice(){
		r1.setPrice(-1);
		assertEquals(r1.getPrice(), 0);
	}
	//toString method
	public void testToString(){
		r1.toString();
	}
	
	
	public void testSetAmtCoffee(){
		r1.setAmtCoffee(-1);
		assertEquals(r1.getAmtCoffee(), 0);
	}
	
	public void testSetAmtChocolate(){
		r1.setAmtChocolate(-1);
		assertEquals(r1.getAmtChocolate(), 0);
	}
	
	public void testSetAmtMilk(){
		r1.setAmtMilk(-1);
		assertEquals(r1.getAmtMilk(), 0);
	}
	
	public void testSetAmtSugar(){
		r1.setAmtSugar(-1);
		assertEquals(r1.getAmtSugar(), 0);
	}
}